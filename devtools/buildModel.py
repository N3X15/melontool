import os
from pathlib import Path
from typing import Any, Dict, Sequence, TextIO

from ruamel.yaml import YAML
yaml = YAML(typ='rt')

class IndentWriter:
    def __init__(self, f: TextIO) -> None:
        self._f: TextIO = f
        self._indentLevel = 0
        self.indentWith = ' '*4
        self._currentIndentChars = ''

    def indent(self, levels: int = 1) -> None:
        self._indentLevel += levels
        self._currentIndentChars = self.indentWith * self._indentLevel

    def dedent(self, levels: int = 1) -> None:
        self.indent(-levels)

    def __enter__(self) -> 'IndentWriter':
        self.indent()
        return self
    def __exit__(self, exc_type, exc_val, exc_tb) -> None:
        self.dedent()

    def writeline(self, input: str) -> 'IndentWriter':
        self._f.write(f'{self._currentIndentChars}{input}\n')
        return self

    def writelines(self, inputs: Sequence[str]) -> 'IndentWriter':
        for input in inputs:
            self._f.write(f'{self._currentIndentChars}{input}\n')
        return self

def makeIndexFile(destfile: Path, files: Dict[str, str]) -> None:
    with open(destfile.with_suffix('.py.new'), 'w') as f:
        w = IndentWriter(f)
        w.writeline('from lxml import etree')
        for module, cls in sorted(files.items()):
            w.writeline(f'from {module} import {cls}')
        w.writeline('lookup = etree.ElementNamespaceClassLookup()')
        w.writeline('namespace = lookup.get_namespace(None)')
        for module, cls in sorted(files.items()):
            w.writeline(f'namespace[{cls!r}] = {cls}')
    os.replace(destfile.with_suffix('.py.new'), destfile)
    print(str(destfile))

def makeModelFile(destfile: Path, config: Dict[str, Any]) -> None:
    clsname = f"Raw{config['name']}"
    with open(destfile.with_suffix('.py.new'), 'w') as f:
        w = IndentWriter(f)
        w.writeline('from melontool.model._base import BaseXMLModel')
        w.writeline('from pathlib import Path')
        w.writeline('from typing import Optional')
        w.writeline(f'__ALL__ = [{clsname!r}]')
        with w.writeline(f'class {clsname}(BaseXMLModel):'):
            if len(config.get('attribs', {})) == 0 and len(sorted(config.get('subelements', {}).items())) == 0:
                w.writeline('pass')
            else:
                for k, data in sorted(config.get('attribs', {}).items()):
                    name = data['name']
                    assert data['type'] in ('String', 'Path', 'Integer', 'Float', 'Bool')
                    fsuffix: str
                    type_: str
                    if data['type'] == 'String':
                        type_, fsuffix = 'str', 'String'
                    elif data['type'] == 'Path':
                        type_, fsuffix = 'Path', 'Path'
                    elif data['type'] == 'Integer':
                        type_, fsuffix = 'int', 'Integer'
                    elif data['type'] == 'Float':
                        type_, fsuffix = 'float', 'Float'
                    elif data['type'] == 'Bool':
                        type_, fsuffix = 'bool', 'Bool'

                    with w.writeline(f'def _get_{k}(self) -> Optional[{type_}]:'):
                        w.writeline(f'return self._getAttr{fsuffix}({name!r})')
                    with w.writeline(f'def _set_{k}(self,  value: Optional[{type_}]) -> None:'):
                        w.writeline(f'self._setAttr{fsuffix}({name!r}, value)')
                    w.writeline(f'{k} = property(_get_{k}, _set_{k})')
                for k, data in sorted(config.get('subelements', {}).items()):
                    name = data['tag']
                    assert data['type'] in ('String', 'Path', 'Integer', 'Float', 'Bool')
                    fsuffix: str
                    type_: str
                    if data['type'] == 'String':
                        type_, fsuffix = 'str', 'String'
                    elif data['type'] == 'Path':
                        type_, fsuffix = 'Path', 'Path'
                    elif data['type'] == 'Integer':
                        type_, fsuffix = 'int', 'Integer'
                    elif data['type'] == 'Float':
                        type_, fsuffix = 'float', 'Float'
                    elif data['type'] == 'Bool':
                        type_, fsuffix = 'bool', 'Bool'

                    with w.writeline(f'def _get_{k}(self) -> Optional[{type_}]:'):
                        w.writeline(f'return self._getE{fsuffix}({name!r})')
                    with w.writeline(f'def _set_{k}(self,  value: Optional[{type_}]) -> None:'):
                        w.writeline(f'self._setE{fsuffix}({name!r}, value)')
                    w.writeline(f'{k} = property(_get_{k}, _set_{k})')
    os.replace(destfile.with_suffix('.py.new'), destfile)
    print(str(destfile))

def main():
    MODEL_PYTHON = Path('melontool') / 'model'
    MODEL_YAML = Path('data') / 'model'
    written = {}
    for modelfile in (MODEL_PYTHON / 'generated').glob('*.py'):
        if modelfile.name == '__init__.py':
            continue
        modelfile.unlink()
    for modelfile in MODEL_YAML.glob('*.yml'):
        with open(modelfile, 'r') as f:
            data = yaml.load(f)
        makeModelFile(MODEL_PYTHON / 'generated' / f'{data["name"].lower()}.py', data)
        written[f'melontool.model.{data["name"].lower()}'] = data["name"]
    makeIndexFile(MODEL_PYTHON / '__init__.py', written)

if __name__ == '__main__':
    main()
