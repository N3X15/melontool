import datetime
import os
from argparse import ArgumentParser, Namespace, _SubParsersAction
from io import StringIO
from pathlib import Path

from babel.core import Locale
from babel.messages.catalog import Catalog
from babel.messages.pofile import read_po, write_po
from buildtools import log, os_utils
from ruamel.yaml import YAML

yaml = YAML(typ='rt')

START_YEAR = 2021
CUR_YEAR = datetime.datetime.now(tz=datetime.timezone.utc).year

SCRIPT_DIR = Path(__file__).parent.parent
LOCALE_DIR = SCRIPT_DIR / 'locale'

LANG = 'en_US'
PYBABEL = os_utils.assertWhich('pybabel')
BUGS_ADDR = 'https://gitlab.com/Scottinator/MelonTool/-/issues/new'
COPYRIGHT = 'MelonTool Contributors'
PROJECT = 'MelonTool'
PROJECT_VERSION = '0.0.1'

def main():
    argp = ArgumentParser(prog='i18n.py', description='Internationalization tool for MelonTool')

    subp: _SubParsersAction = argp.add_subparsers()

    _register_add(subp)
    _register_fix(subp)
    _register_dump(subp)
    _register_update(subp)

    argp.epilog = '''
To add:
    python devtools/i18n.py add <melontool.domain> [--locale=en_US|etc]

To update:
    pybabel extract -o guess.pot <melontool.domain> 
    python devtools/i18n.py update <melontool.domain> guess.pot [--locale=en_US|etc]
'''

    args: Namespace = argp.parse_args()

    if not hasattr(args, 'cmd'):
        argp.print_usage()
    args.cmd(args)


def getPathOfPofile(locale: Locale, domain: str) -> Path:
    return LOCALE_DIR / str(locale) / 'LC_MESSAGES' / f'{domain}.po'


def _register_add(subp: _SubParsersAction) -> None:
    p = subp.add_parser('add')
    p.add_argument('domain', type=str)
    p.add_argument('--locale', type=str, default='en_US')
    p.add_argument('--filename', type=Path, default=None)
    p.set_defaults(cmd=cmd_add)


def cmd_add(args: Namespace) -> None:
    if args.filename is not None:
        infile = args.filename
    else:
        infile = SCRIPT_DIR / os.sep.join(args.domain.split('.'))
        if infile.with_suffix('.py').is_file():
            infile = infile.with_suffix('.py')
        else:
            infile = infile / '__init__.py'
    os_utils.cmd([PYBABEL, 'extract',
                  f'--msgid-bugs-address={BUGS_ADDR}',
                  f'--copyright-holder={COPYRIGHT}',
                  f'--project={PROJECT}',
                  f'--version={PROJECT_VERSION}',
                  '-o', str(SCRIPT_DIR / 'guess.pot'),
                  str(infile)
                  ], echo=True, show_output=True, critical=True)
    os_utils.cmd([PYBABEL, 'init',
                  '-d', LOCALE_DIR,
                  '-l', args.locale,
                  '-D', args.domain,
                  '-i', 'guess.pot'
                  ], echo=True, show_output=True, critical=True)

    locale: Locale = Locale.parse(args.locale, sep='_')
    domain: str = args.domain
    fixPoFile(locale, domain)


def fixPoFile(locale: Locale, domain: str) -> None:
    pofile: Path = getPathOfPofile(locale, domain)
    c: Catalog
    with pofile.open('r') as f:
        c = read_po(f, locale, domain)

    c.copyright_holder = COPYRIGHT
    copy_year = str(
        CUR_YEAR) if CUR_YEAR == START_YEAR else f'{START_YEAR}-{CUR_YEAR}'
    c.project = PROJECT
    c.project_version = PROJECT_VERSION
    c.version = PROJECT_VERSION
    c.msgid_bugs_address = BUGS_ADDR
    c.header_comment = f'''# {locale.display_name} translations for {PROJECT}
#
# Copyright {copy_year} {c.copyright_holder}
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# 
# Please use POEdit or similar to edit this file, then run:
# `python devtools/i18n.py fix <domain> [--lang=en_US|ru_RU|etc]`
#'''
    c.language_team = COPYRIGHT
    c.last_translator = COPYRIGHT

    with open(pofile, 'wb') as f:
        write_po(f, c)


def _register_dump(subp: _SubParsersAction) -> None:
    p = subp.add_parser('dump')
    p.add_argument('domain', type=str)
    p.add_argument('--locale', type=str, default='en_US')
    p.set_defaults(cmd=cmd_dump)


def cmd_dump(args: Namespace) -> None:
    locale: Locale = Locale.parse(args.locale, sep='_')
    domain: str = args.domain
    pofile: Path = getPathOfPofile(locale, domain)
    c: Catalog
    with pofile.open('r') as f:
        c = read_po(f, locale, domain)
    data = {
        'charset': c.charset,
        'copyright_holder': c.copyright_holder,
        'creation_date': c.creation_date,
        'domain': c.domain,
        'fuzzy': c.fuzzy,
        'header_comment': c.header_comment,
        'language_team': c.language_team,
        'last_translator': c.last_translator,
        #'locale': c.locale,
        'locale_identifier': c.locale_identifier,
        'obsolete': c.obsolete,
        'project': c.project,
        'revision_date': c.revision_date,
        'version': c.version
    }
    f = StringIO()
    yaml.dump(data, f)
    f.seek(0)
    print(f.read())


def _register_fix(subp: _SubParsersAction) -> None:
    p = subp.add_parser('fix')
    p.add_argument('domain', type=str)
    p.add_argument('--locale', type=str, default='en_US')
    p.set_defaults(cmd=cmd_fix)


def cmd_fix(args: Namespace) -> None:
    locale: Locale = Locale.parse(args.locale, sep='_')
    domain: str = args.domain
    fixPoFile(locale, domain)


def _register_update(subp: _SubParsersAction) -> None:
    p = subp.add_parser('update')
    p.add_argument('domain', type=str)
    p.add_argument('inputfile', type=Path)
    p.add_argument('--locale', type=str, default='en_US')
    p.set_defaults(cmd=cmd_update)


def cmd_update(args: Namespace) -> None:
    locale: Locale = Locale.parse(args.locale, sep='_')
    domain: str = args.domain
    inputfile: Path = args.inputfile
    if not inputfile.is_file():
        log.error('File %s does not exist', inputfile)
        return
    pofile: Path = getPathOfPofile(locale, domain)
    c: Catalog
    with pofile.open('r') as f:
        c = read_po(f, locale, domain)
    with inputfile.open('r') as f:
        c.update(read_po(f))

    with open(pofile, 'wb') as f:
        write_po(f, c)

    fixPoFile(locale, domain)


if __name__ == '__main__':
    main()
