from melontool.model.generated.reference import RawReference

__ALL__ = ['Reference']
class Reference(RawReference):
    pass