from melontool.model._base import BaseXMLModel
from pathlib import Path
from typing import Optional
__ALL__ = ['RawEmbeddedResource']
class RawEmbeddedResource(BaseXMLModel):
    def _get_include(self) -> Optional[Path]:
        return self._getAttrPath('Include')
    def _set_include(self,  value: Optional[Path]) -> None:
        self._setAttrPath('Include', value)
    include = property(_get_include, _set_include)
