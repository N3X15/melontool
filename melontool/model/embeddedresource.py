from melontool.model.generated.embeddedresource import RawEmbeddedResource
__ALL__ = ["EmbeddedResource"]
class EmbeddedResource(RawEmbeddedResource):
    pass
