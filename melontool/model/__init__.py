from lxml import etree
from melontool.model.embeddedresource import EmbeddedResource
from melontool.model.project import Project
from melontool.model.reference import Reference
lookup = etree.ElementNamespaceClassLookup()
namespace = lookup.get_namespace(None)
namespace['EmbeddedResource'] = EmbeddedResource
namespace['Project'] = Project
namespace['Reference'] = Reference
