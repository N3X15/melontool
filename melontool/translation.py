from typing import Dict, List, Optional, Union
from enum import IntEnum, auto

import click
from pycsbinarywriter import cstypes
import uuid, string
from pathlib import Path
from melontool.indentwriter import IndentWriter
class ELangID(IntEnum):
    af_NA = auto() # Afrikaans (Namibia)
    af_ZA = auto() # Afrikaans (South Africa)
    af = auto() # Afrikaans
    ak_GH = auto() # Akan (Ghana)
    ak = auto() # Akan
    sq_AL = auto() # Albanian (Albania)
    sq = auto() # Albanian
    am_ET = auto() # Amharic (Ethiopia)
    am = auto() # Amharic
    ar_DZ = auto() # Arabic (Algeria)
    ar_BH = auto() # Arabic (Bahrain)
    ar_EG = auto() # Arabic (Egypt)
    ar_IQ = auto() # Arabic (Iraq)
    ar_JO = auto() # Arabic (Jordan)
    ar_KW = auto() # Arabic (Kuwait)
    ar_LB = auto() # Arabic (Lebanon)
    ar_LY = auto() # Arabic (Libya)
    ar_MA = auto() # Arabic (Morocco)
    ar_OM = auto() # Arabic (Oman)
    ar_QA = auto() # Arabic (Qatar)
    ar_SA = auto() # Arabic (Saudi Arabia)
    ar_SD = auto() # Arabic (Sudan)
    ar_SY = auto() # Arabic (Syria)
    ar_TN = auto() # Arabic (Tunisia)
    ar_AE = auto() # Arabic (United Arab Emirates)
    ar_YE = auto() # Arabic (Yemen)
    ar = auto() # Arabic
    hy_AM = auto() # Armenian (Armenia)
    hy = auto() # Armenian
    as_IN = auto() # Assamese (India)
    AS = auto() # Assamese
    asa_TZ = auto() # Asu (Tanzania)
    asa = auto() # Asu
    az_Cyrl = auto() # Azerbaijani (Cyrillic)
    az_Cyrl_AZ = auto() # Azerbaijani (Cyrillic, Azerbaijan)
    az_Latn = auto() # Azerbaijani (Latin)
    az_Latn_AZ = auto() # Azerbaijani (Latin, Azerbaijan)
    az = auto() # Azerbaijani
    bm_ML = auto() # Bambara (Mali)
    bm = auto() # Bambara
    eu_ES = auto() # Basque (Spain)
    eu = auto() # Basque
    be_BY = auto() # Belarusian (Belarus)
    be = auto() # Belarusian
    bem_ZM = auto() # Bemba (Zambia)
    bem = auto() # Bemba
    bez_TZ = auto() # Bena (Tanzania)
    bez = auto() # Bena
    bn_BD = auto() # Bengali (Bangladesh)
    bn_IN = auto() # Bengali (India)
    bn = auto() # Bengali
    bs_BA = auto() # Bosnian (Bosnia and Herzegovina)
    bs = auto() # Bosnian
    bg_BG = auto() # Bulgarian (Bulgaria)
    bg = auto() # Bulgarian
    my_MM = auto() # Burmese (Myanmar [Burma])
    my = auto() # Burmese
    yue_Hant_HK = auto() # Cantonese (Traditional, Hong Kong SAR China)
    ca_ES = auto() # Catalan (Spain)
    ca = auto() # Catalan
    tzm_Latn = auto() # Central Morocco Tamazight (Latin)
    tzm_Latn_MA = auto() # Central Morocco Tamazight (Latin, Morocco)
    tzm = auto() # Central Morocco Tamazight
    chr_US = auto() # Cherokee (United States)
    chr = auto() # Cherokee
    cgg_UG = auto() # Chiga (Uganda)
    cgg = auto() # Chiga
    zh_Hans = auto() # Chinese (Simplified Han)
    zh_Hans_CN = auto() # Chinese (Simplified Han, China)
    zh_Hans_HK = auto() # Chinese (Simplified Han, Hong Kong SAR China)
    zh_Hans_MO = auto() # Chinese (Simplified Han, Macau SAR China)
    zh_Hans_SG = auto() # Chinese (Simplified Han, Singapore)
    zh_Hant = auto() # Chinese (Traditional Han)
    zh_Hant_HK = auto() # Chinese (Traditional Han, Hong Kong SAR China)
    zh_Hant_MO = auto() # Chinese (Traditional Han, Macau SAR China)
    zh_Hant_TW = auto() # Chinese (Traditional Han, Taiwan)
    zh = auto() # Chinese
    kw_GB = auto() # Cornish (United Kingdom)
    kw = auto() # Cornish
    hr_HR = auto() # Croatian (Croatia)
    hr = auto() # Croatian
    cs_CZ = auto() # Czech (Czech Republic)
    cs = auto() # Czech
    da_DK = auto() # Danish (Denmark)
    da = auto() # Danish
    nl_BE = auto() # Dutch (Belgium)
    nl_NL = auto() # Dutch (Netherlands)
    nl = auto() # Dutch
    ebu_KE = auto() # Embu (Kenya)
    ebu = auto() # Embu
    en_AS = auto() # English (American Samoa)
    en_AU = auto() # English (Australia)
    en_BE = auto() # English (Belgium)
    en_BZ = auto() # English (Belize)
    en_BW = auto() # English (Botswana)
    en_CA = auto() # English (Canada)
    en_GU = auto() # English (Guam)
    en_HK = auto() # English (Hong Kong SAR China)
    en_IN = auto() # English (India)
    en_IE = auto() # English (Ireland)
    en_IL = auto() # English (Israel)
    en_JM = auto() # English (Jamaica)
    en_MT = auto() # English (Malta)
    en_MH = auto() # English (Marshall Islands)
    en_MU = auto() # English (Mauritius)
    en_NA = auto() # English (Namibia)
    en_NZ = auto() # English (New Zealand)
    en_MP = auto() # English (Northern Mariana Islands)
    en_PK = auto() # English (Pakistan)
    en_PH = auto() # English (Philippines)
    en_SG = auto() # English (Singapore)
    en_ZA = auto() # English (South Africa)
    en_TT = auto() # English (Trinidad and Tobago)
    en_UM = auto() # English (U.S. Minor Outlying Islands)
    en_VI = auto() # English (U.S. Virgin Islands)
    en_GB = auto() # English (United Kingdom)
    en_US = auto() # English (United States)
    en_ZW = auto() # English (Zimbabwe)
    en = auto() # English
    eo = auto() # Esperanto
    et_EE = auto() # Estonian (Estonia)
    et = auto() # Estonian
    ee_GH = auto() # Ewe (Ghana)
    ee_TG = auto() # Ewe (Togo)
    ee = auto() # Ewe
    fo_FO = auto() # Faroese (Faroe Islands)
    fo = auto() # Faroese
    fil_PH = auto() # Filipino (Philippines)
    fil = auto() # Filipino
    fi_FI = auto() # Finnish (Finland)
    fi = auto() # Finnish
    fr_BE = auto() # French (Belgium)
    fr_BJ = auto() # French (Benin)
    fr_BF = auto() # French (Burkina Faso)
    fr_BI = auto() # French (Burundi)
    fr_CM = auto() # French (Cameroon)
    fr_CA = auto() # French (Canada)
    fr_CF = auto() # French (Central African Republic)
    fr_TD = auto() # French (Chad)
    fr_KM = auto() # French (Comoros)
    fr_CG = auto() # French (Congo - Brazzaville)
    fr_CD = auto() # French (Congo - Kinshasa)
    fr_CI = auto() # French (Côte d’Ivoire)
    fr_DJ = auto() # French (Djibouti)
    fr_GQ = auto() # French (Equatorial Guinea)
    fr_FR = auto() # French (France)
    fr_GA = auto() # French (Gabon)
    fr_GP = auto() # French (Guadeloupe)
    fr_GN = auto() # French (Guinea)
    fr_LU = auto() # French (Luxembourg)
    fr_MG = auto() # French (Madagascar)
    fr_ML = auto() # French (Mali)
    fr_MQ = auto() # French (Martinique)
    fr_MC = auto() # French (Monaco)
    fr_NE = auto() # French (Niger)
    fr_RW = auto() # French (Rwanda)
    fr_RE = auto() # French (Réunion)
    fr_BL = auto() # French (Saint Barthélemy)
    fr_MF = auto() # French (Saint Martin)
    fr_SN = auto() # French (Senegal)
    fr_CH = auto() # French (Switzerland)
    fr_TG = auto() # French (Togo)
    fr = auto() # French
    ff_SN = auto() # Fulah (Senegal)
    ff = auto() # Fulah
    gl_ES = auto() # Galician (Spain)
    gl = auto() # Galician
    lg_UG = auto() # Ganda (Uganda)
    lg = auto() # Ganda
    ka_GE = auto() # Georgian (Georgia)
    ka = auto() # Georgian
    de_AT = auto() # German (Austria)
    de_BE = auto() # German (Belgium)
    de_DE = auto() # German (Germany)
    de_LI = auto() # German (Liechtenstein)
    de_LU = auto() # German (Luxembourg)
    de_CH = auto() # German (Switzerland)
    de = auto() # German
    el_CY = auto() # Greek (Cyprus)
    el_GR = auto() # Greek (Greece)
    el = auto() # Greek
    gu_IN = auto() # Gujarati (India)
    gu = auto() # Gujarati
    guz_KE = auto() # Gusii (Kenya)
    guz = auto() # Gusii
    ha_Latn = auto() # Hausa (Latin)
    ha_Latn_GH = auto() # Hausa (Latin, Ghana)
    ha_Latn_NE = auto() # Hausa (Latin, Niger)
    ha_Latn_NG = auto() # Hausa (Latin, Nigeria)
    ha = auto() # Hausa
    haw_US = auto() # Hawaiian (United States)
    haw = auto() # Hawaiian
    he_IL = auto() # Hebrew (Israel)
    he = auto() # Hebrew
    hi_IN = auto() # Hindi (India)
    hi = auto() # Hindi
    hu_HU = auto() # Hungarian (Hungary)
    hu = auto() # Hungarian
    is_IS = auto() # Icelandic (Iceland)
    #is = auto() # Icelandic
    ig_NG = auto() # Igbo (Nigeria)
    ig = auto() # Igbo
    id_ID = auto() # Indonesian (Indonesia)
    id = auto() # Indonesian
    ga_IE = auto() # Irish (Ireland)
    ga = auto() # Irish
    it_IT = auto() # Italian (Italy)
    it_CH = auto() # Italian (Switzerland)
    it = auto() # Italian
    ja_JP = auto() # Japanese (Japan)
    ja = auto() # Japanese
    kea_CV = auto() # Kabuverdianu (Cape Verde)
    kea = auto() # Kabuverdianu
    kab_DZ = auto() # Kabyle (Algeria)
    kab = auto() # Kabyle
    kl_GL = auto() # Kalaallisut (Greenland)
    kl = auto() # Kalaallisut
    kln_KE = auto() # Kalenjin (Kenya)
    kln = auto() # Kalenjin
    kam_KE = auto() # Kamba (Kenya)
    kam = auto() # Kamba
    kn_IN = auto() # Kannada (India)
    kn = auto() # Kannada
    kk_Cyrl = auto() # Kazakh (Cyrillic)
    kk_Cyrl_KZ = auto() # Kazakh (Cyrillic, Kazakhstan)
    kk = auto() # Kazakh
    km_KH = auto() # Khmer (Cambodia)
    km = auto() # Khmer
    ki_KE = auto() # Kikuyu (Kenya)
    ki = auto() # Kikuyu
    rw_RW = auto() # Kinyarwanda (Rwanda)
    rw = auto() # Kinyarwanda
    kok_IN = auto() # Konkani (India)
    kok = auto() # Konkani
    ko_KR = auto() # Korean (South Korea)
    ko = auto() # Korean
    khq_ML = auto() # Koyra Chiini (Mali)
    khq = auto() # Koyra Chiini
    ses_ML = auto() # Koyraboro Senni (Mali)
    ses = auto() # Koyraboro Senni
    lag_TZ = auto() # Langi (Tanzania)
    lag = auto() # Langi
    lv_LV = auto() # Latvian (Latvia)
    lv = auto() # Latvian
    lt_LT = auto() # Lithuanian (Lithuania)
    lt = auto() # Lithuanian
    luo_KE = auto() # Luo (Kenya)
    luo = auto() # Luo
    luy_KE = auto() # Luyia (Kenya)
    luy = auto() # Luyia
    mk_MK = auto() # Macedonian (Macedonia)
    mk = auto() # Macedonian
    jmc_TZ = auto() # Machame (Tanzania)
    jmc = auto() # Machame
    kde_TZ = auto() # Makonde (Tanzania)
    kde = auto() # Makonde
    mg_MG = auto() # Malagasy (Madagascar)
    mg = auto() # Malagasy
    ms_BN = auto() # Malay (Brunei)
    ms_MY = auto() # Malay (Malaysia)
    ms = auto() # Malay
    ml_IN = auto() # Malayalam (India)
    ml = auto() # Malayalam
    mt_MT = auto() # Maltese (Malta)
    mt = auto() # Maltese
    gv_GB = auto() # Manx (United Kingdom)
    gv = auto() # Manx
    mr_IN = auto() # Marathi (India)
    mr = auto() # Marathi
    mas_KE = auto() # Masai (Kenya)
    mas_TZ = auto() # Masai (Tanzania)
    mas = auto() # Masai
    mer_KE = auto() # Meru (Kenya)
    mer = auto() # Meru
    mfe_MU = auto() # Morisyen (Mauritius)
    mfe = auto() # Morisyen
    naq_NA = auto() # Nama (Namibia)
    naq = auto() # Nama
    ne_IN = auto() # Nepali (India)
    ne_NP = auto() # Nepali (Nepal)
    ne = auto() # Nepali
    nd_ZW = auto() # North Ndebele (Zimbabwe)
    nd = auto() # North Ndebele
    nb_NO = auto() # Norwegian Bokmål (Norway)
    nb = auto() # Norwegian Bokmål
    nn_NO = auto() # Norwegian Nynorsk (Norway)
    nn = auto() # Norwegian Nynorsk
    nyn_UG = auto() # Nyankole (Uganda)
    nyn = auto() # Nyankole
    or_IN = auto() # Oriya (India)
    #or = auto() # Oriya
    om_ET = auto() # Oromo (Ethiopia)
    om_KE = auto() # Oromo (Kenya)
    om = auto() # Oromo
    ps_AF = auto() # Pashto (Afghanistan)
    ps = auto() # Pashto
    fa_AF = auto() # Persian (Afghanistan)
    fa_IR = auto() # Persian (Iran)
    fa = auto() # Persian
    pl_PL = auto() # Polish (Poland)
    pl = auto() # Polish
    pt_BR = auto() # Portuguese (Brazil)
    pt_GW = auto() # Portuguese (Guinea-Bissau)
    pt_MZ = auto() # Portuguese (Mozambique)
    pt_PT = auto() # Portuguese (Portugal)
    pt = auto() # Portuguese
    pa_Arab = auto() # Punjabi (Arabic)
    pa_Arab_PK = auto() # Punjabi (Arabic, Pakistan)
    pa_Guru = auto() # Punjabi (Gurmukhi)
    pa_Guru_IN = auto() # Punjabi (Gurmukhi, India)
    pa = auto() # Punjabi
    ro_MD = auto() # Romanian (Moldova)
    ro_RO = auto() # Romanian (Romania)
    ro = auto() # Romanian
    rm_CH = auto() # Romansh (Switzerland)
    rm = auto() # Romansh
    rof_TZ = auto() # Rombo (Tanzania)
    rof = auto() # Rombo
    ru_MD = auto() # Russian (Moldova)
    ru_RU = auto() # Russian (Russia)
    ru_UA = auto() # Russian (Ukraine)
    ru = auto() # Russian
    rwk_TZ = auto() # Rwa (Tanzania)
    rwk = auto() # Rwa
    saq_KE = auto() # Samburu (Kenya)
    saq = auto() # Samburu
    sg_CF = auto() # Sango (Central African Republic)
    sg = auto() # Sango
    seh_MZ = auto() # Sena (Mozambique)
    seh = auto() # Sena
    sr_Cyrl = auto() # Serbian (Cyrillic)
    sr_Cyrl_BA = auto() # Serbian (Cyrillic, Bosnia and Herzegovina)
    sr_Cyrl_ME = auto() # Serbian (Cyrillic, Montenegro)
    sr_Cyrl_RS = auto() # Serbian (Cyrillic, Serbia)
    sr_Latn = auto() # Serbian (Latin)
    sr_Latn_BA = auto() # Serbian (Latin, Bosnia and Herzegovina)
    sr_Latn_ME = auto() # Serbian (Latin, Montenegro)
    sr_Latn_RS = auto() # Serbian (Latin, Serbia)
    sr = auto() # Serbian
    sn_ZW = auto() # Shona (Zimbabwe)
    sn = auto() # Shona
    ii_CN = auto() # Sichuan Yi (China)
    ii = auto() # Sichuan Yi
    si_LK = auto() # Sinhala (Sri Lanka)
    si = auto() # Sinhala
    sk_SK = auto() # Slovak (Slovakia)
    sk = auto() # Slovak
    sl_SI = auto() # Slovenian (Slovenia)
    sl = auto() # Slovenian
    xog_UG = auto() # Soga (Uganda)
    xog = auto() # Soga
    so_DJ = auto() # Somali (Djibouti)
    so_ET = auto() # Somali (Ethiopia)
    so_KE = auto() # Somali (Kenya)
    so_SO = auto() # Somali (Somalia)
    so = auto() # Somali
    es_AR = auto() # Spanish (Argentina)
    es_BO = auto() # Spanish (Bolivia)
    es_CL = auto() # Spanish (Chile)
    es_CO = auto() # Spanish (Colombia)
    es_CR = auto() # Spanish (Costa Rica)
    es_DO = auto() # Spanish (Dominican Republic)
    es_EC = auto() # Spanish (Ecuador)
    es_SV = auto() # Spanish (El Salvador)
    es_GQ = auto() # Spanish (Equatorial Guinea)
    es_GT = auto() # Spanish (Guatemala)
    es_HN = auto() # Spanish (Honduras)
    es_419 = auto() # Spanish (Latin America)
    es_MX = auto() # Spanish (Mexico)
    es_NI = auto() # Spanish (Nicaragua)
    es_PA = auto() # Spanish (Panama)
    es_PY = auto() # Spanish (Paraguay)
    es_PE = auto() # Spanish (Peru)
    es_PR = auto() # Spanish (Puerto Rico)
    es_ES = auto() # Spanish (Spain)
    es_US = auto() # Spanish (United States)
    es_UY = auto() # Spanish (Uruguay)
    es_VE = auto() # Spanish (Venezuela)
    es = auto() # Spanish
    sw_KE = auto() # Swahili (Kenya)
    sw_TZ = auto() # Swahili (Tanzania)
    sw = auto() # Swahili
    sv_FI = auto() # Swedish (Finland)
    sv_SE = auto() # Swedish (Sweden)
    sv = auto() # Swedish
    gsw_CH = auto() # Swiss German (Switzerland)
    gsw = auto() # Swiss German
    shi_Latn = auto() # Tachelhit (Latin)
    shi_Latn_MA = auto() # Tachelhit (Latin, Morocco)
    shi_Tfng = auto() # Tachelhit (Tifinagh)
    shi_Tfng_MA = auto() # Tachelhit (Tifinagh, Morocco)
    shi = auto() # Tachelhit
    dav_KE = auto() # Taita (Kenya)
    dav = auto() # Taita
    ta_IN = auto() # Tamil (India)
    ta_LK = auto() # Tamil (Sri Lanka)
    ta = auto() # Tamil
    te_IN = auto() # Telugu (India)
    te = auto() # Telugu
    teo_KE = auto() # Teso (Kenya)
    teo_UG = auto() # Teso (Uganda)
    teo = auto() # Teso
    th_TH = auto() # Thai (Thailand)
    th = auto() # Thai
    bo_CN = auto() # Tibetan (China)
    bo_IN = auto() # Tibetan (India)
    bo = auto() # Tibetan
    ti_ER = auto() # Tigrinya (Eritrea)
    ti_ET = auto() # Tigrinya (Ethiopia)
    ti = auto() # Tigrinya
    to_TO = auto() # Tonga (Tonga)
    to = auto() # Tonga
    tr_TR = auto() # Turkish (Turkey)
    tr = auto() # Turkish
    uk_UA = auto() # Ukrainian (Ukraine)
    uk = auto() # Ukrainian
    ur_IN = auto() # Urdu (India)
    ur_PK = auto() # Urdu (Pakistan)
    ur = auto() # Urdu
    uz_Arab = auto() # Uzbek (Arabic)
    uz_Arab_AF = auto() # Uzbek (Arabic, Afghanistan)
    uz_Cyrl = auto() # Uzbek (Cyrillic)
    uz_Cyrl_UZ = auto() # Uzbek (Cyrillic, Uzbekistan)
    uz_Latn = auto() # Uzbek (Latin)
    uz_Latn_UZ = auto() # Uzbek (Latin, Uzbekistan)
    uz = auto() # Uzbek
    vi_VN = auto() # Vietnamese (Vietnam)
    vi = auto() # Vietnamese
    vun_TZ = auto() # Vunjo (Tanzania)
    vun = auto() # Vunjo
    cy_GB = auto() # Welsh (United Kingdom)
    cy = auto() # Welsh
    yo_NG = auto() # Yoruba (Nigeria)
    yo = auto() # Yoruba
    zu_ZA = auto() # Zulu (South Africa)
    zu = auto() # Zulu

class I18NKey:
    def __init__(self) -> None:
        self.idx: int = 0
        self.id: str = str(uuid.uuid4())
        self.comment: Optional[str] = None
        self.langs: Dict[ELangID, str] = {}

    def asDict(self) -> dict:
        o = {
            'id': self.id
        }
        if self.comment is not None:
            o['comment'] = self.comment
        o['langs'] = {k.name: v for k, v in self.langs.items()}
        return o

    def fromDict(self, data: dict) -> None:
        self.id = data['id']
        assert isinstance(self.id, int)
        assert len(data['langs']) > 0
        for k, v in data['langs'].items():
            self.langs[ELangID(k)] = v
        if 'comment' in data:
            assert isinstance(data['comment'], str)
            self.comment = data['comment']

    def asBytes(self) -> bytes:
        o: bytes = cstypes.uint16.pack(self.id)
        o += cstypes.string.pack(self.comment if self.comment is not None else '')
        o += cstypes.uint16.pack(len(self.lang))
        for langid, v in self.langs.items():
            o += cstypes.uint16.pack(langid.value)
            o += cstypes.string.pack(v)
        return o

class I18NBuilder:
    def __init__(self) -> None:
        self.keys: List[I18NKey] = {}
        self.idsByIdent: Dict[str, int] = {}
    
    def get(self, id: Union[str, int]) -> I18NKey:
        return self.keys[id] if isinstance(id, int) else self.keys[self.idsByIdent[id]]
    
    def create(self, lang: ELangID, ident: str, value: str) -> I18NKey:
        key = I18NKey()
        key.idx = len(self.keys)
        key.id = ident
        key.langs[lang] = value
        self.keys.append(key)
        self.idsByIdent[key.ident] = key.idx
        return key

    def value2Str(self, value: str) -> None:
        key: str = ''
        for c in value:
            if c in string.alphanum:
                key += c.upper()
            else:
                if key[-1] == '_':
                    continue
                key += '_'
        if key[0] in string.digits:
            key = '_'+key
        return key

    def createFromEnUSValue(self, value: str) -> I18NKey:
        return self.create(ELangID.en_US, self.value2Str(value), value)

    def ident_exists(self, ident: str) -> bool:
        return ident in self.idsByIdent
    def ident_and_lang_exist(self, lang: ELangID, ident: str) -> bool:
        return ident in self.idsByIdent and lang in self.keys[self.idsByIdent[ident]].langs

    def index_exists(self, index: int) -> bool:
        return 0 <= index < len(self.keys)
    def index_and_lang_exist(self, lang: ELangID, index: int) -> bool:
        return self.index_exists(index) and lang in self.keys[index].langs

    def set(self, lang: ELangID, index_or_ident: Union[str, int], value: str) -> None:
        if isinstance(index_or_ident, int):
            if not self.index_exists(index_or_ident):
                self.create(lang, self.value2Str(value), value)
                return
        else:
            if not self.key_exists(index_or_ident):
                self.create(lang, index_or_ident, value)
                return
        self.get(index_or_ident).langs[lang] = value

    def delete_key(self, ident: str) -> None:
        key: I18NKey = self.get(ident)
        del self.keys[key.idx]
        del self.idsByIdent[key.id]
        self.recalcIndexes()

    def delete(self, lang: ELangID, ident: str) -> None:
        del self.keys[ident].langs[self.ident]
        if len(self.keys[ident].langs) == 0:
            self.delete_key(ident)

    def recalcIndexes(self) -> None:
        self.idsByIdent = {}
        for i, v in enumerate(self.keys):
            v.idx = i
            self.idsByIdent[v.id] = v.idx
    
    def asDict(self) -> Dict[str, dict]:
        return {v.id: v.asDict() for v in self.keys}

    def fromDict(self, data: dict) -> None:
        self.keys=[None]*(max([x.idx for x in data.values()])+1)
        self.idsByIdent={}
        for v in data.values():
            key = I18NKey()
            key.fromDict(v)
            self.keys[key.idx] = key
            self.idsByIdent[key.id] = key.idx
        nk = []
        needRecalc=False
        for i, key in enumerate(self.keys):
            if key is None:
                click.secho(f'W: self.keys[{i}] is None.  Skipped.', fg='yellow')
                needRecalc=True
            nk.append(key)
        self.keys = nk
        if needRecalc:
            click.secho('W: Need to recalc indices.  Running...', fg='yellow')
            self.recalcIndexes()
            click.secho('    OK',fg='green')

    def asBytes(self) -> bytes:
        o = b''
        o += cstypes.uint32.pack(len(self.keys))
        for key in self.keys:
            o += key.asBytes()
        return o

    def genEnum(self, filename: Path, className: str, namespace: str, indentChar: str = '    ') -> None:
        with filename.open('w') as f:
            w = IndentWriter(f, indent_chars=indentChar)
            with w.writeline(f'namespace {namespace} {{'):
                with w.writeline(f'public enum {className}: ushort {{'):
                    lastIndex=len(self.keys)-1
                    comma = ','
                    for i, key in enumerate(self.keys):
                        if i == lastIndex:
                            comma = ''
                        w.writeline(f'{key.id.upper()} = {key.idx}{comma}')
                w.writeline('}')
            w.writeline('}')

    