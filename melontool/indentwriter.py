
class IndentWriter(object):
    def __init__(self, fh, indent_level=0, indent_chars='\t', variables={}):
        self._f = fh
        self.indent_level = indent_level
        self.indent_chars = indent_chars
        self.variables = variables

    def writeline(self, string=''):
        if string == '':
            self._f.write('\n')
        else:
            self._f.write('{}{}\n'.format(
                (self.indent_chars * self.indent_level), self.format(string)))
        return self

    def format(self, string):
        for key, value in self.variables.items():
            string = string.replace('{{{}}}'.format(key), str(value))
        return string

    def __enter__(self):
        self.indent_level += 1

    def __exit__(self, type, value, traceback):
        self.indent_level -= 1
