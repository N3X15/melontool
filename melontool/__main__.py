import argparse
import os
import re
import shlex
import shutil
import subprocess
import time
from pathlib import Path
from typing import Optional, Sequence

import click
import humanfriendly
from lxml import etree
from ruamel.yaml import YAML

from melontool.consts import PROJCFG_FILE, LOCALPROJCFG_FILE, DEFAULT_DNF, BREAKS_SHIT
from melontool.gameids import EGameID
from melontool.i18n import getTxnFor  # isort: skip
from melontool.model.reference import Reference
from melontool.projectconfig import ProjectConfig, EProjectType
from melontool.steamlibrary import SteamApp, SteamLibrary
from melontool.utils import existingDirPath, existingFilePath

t, _ = getTxnFor("melontool.__main__")


yaml = YAML(typ="rt", pure=True)
GAME_APP_ID: int = 438100
GAME_DIR: Path = None


def all_games() -> Path:
    steam = SteamLibrary()
    steam.load()
    return {k: a.path for k, a in steam.games.items()}


def getSigForDLL(dllpath: Path) -> str:
    res = subprocess.run(
        [
            "powershell",
            "-Command",
            f"& {{([system.reflection.assembly]::loadfile({str(dllpath)!r})).FullName}}",
        ],
        capture_output=True,
    )
    print(res.stdout, res.stderr)
    o = res.stdout.decode("utf-8").strip()
    e = res.stderr.decode("utf-8").strip()
    if m := re.search(r"in assembly '([^']+)'", e):
        return m[1]
    assert len(o)
    return o


def main(argv: Optional[Sequence[str]] = None):
    argp = argparse.ArgumentParser(
        prog="melontool", description=_("A tool for juggling your melons.")
    )
    subp = argp.add_subparsers()
    _reg_add(subp)
    _reg_build(subp)
    _reg_convert(subp)
    _reg_deploy(subp)
    _reg_generate(subp)
    _reg_init(subp)

    args = argp.parse_args(argv)
    if hasattr(args, "cmd"):
        args.cmd(args)
    else:
        argp.print_help()


def _reg_init(subp: argparse._SubParsersAction) -> None:
    cmdp = subp.add_parser("init", aliases=["i"])
    cmdp.add_argument(
        "--csproj", "--cs-proj", "-C", type=Path, help=_("Path to csproj file.")
    )
    cmdp.add_argument(
        "--steam-app-name",
        "-N",
        choices=[x.name.lower() for x in EGameID if x.value is not None],
        help=_("Steam App name of the target game"),
        default=None,
        nargs="?",
    )
    cmdp.add_argument(
        "--steam-app-id",
        "-I",
        type=int,
        help=_("Steam AppId of the target game"),
        default=None,
        nargs="?",
    )
    cmdp.add_argument(
        "--game-path",
        "-P",
        type=existingDirPath("--game-path/-I"),
        help=_("Installation path of the target game"),
        default=None,
        nargs="?",
    )
    cmdp.set_defaults(cmd=cmd_init)


def cmd_init(args: argparse.Namespace) -> None:
    steam = SteamLibrary()
    steam.load()
    app: SteamApp = None
    if args.steam_app_id is not None:
        app = steam.get(args.steam_app_id)
    if args.steam_app_name is not None:
        app = steam.get(EGameID[args.steam_app_name.upper()].value.appid)
    if app is None:
        click.secho(
            _("Unable to find game by the criteria you provided.  Please try again."),
            fg="red",
        )
        return
    cfg = ProjectConfig()
    cfg.game = app
    cfg.set_appid(app.id)
    cfg.csproj = args.csproj
    cfg.save(PROJCFG_FILE)
    if args.game_path is not None:
        cfg.localConfig.game_path = args.game_path
    cfg.saveLocalConfigTo(LOCALPROJCFG_FILE)
    cmd = ["dotnet", "new", "classlib", "--framework", DEFAULT_DNF]
    click.echo(
        _("Running {CMD}...").format(CMD=click.style(shlex.join(cmd), fg="yellow"))
    )
    subprocess.run(cmd)


def _reg_build(subp: argparse._SubParsersAction) -> None:
    cmdp = subp.add_parser("build", help=_("Compiles the DLL."))
    cmdp.add_argument(
        "--verbose", "-v", action="store_true", default=False, help=_("Verbose")
    )
    cmdp.add_argument(
        "--deploy", action="store_true", default=False, help=_("Deploy after building")
    )
    cmdp.add_argument(
        "--basedir",
        "--base-dir",
        "--melonloader.basedir",
        "-B",
        type=str,
        default=None,
        help=_("See --melonloader.basedir (https://github.com/LavaGang/MelonLoader)"),
    )
    cmdp.set_defaults(cmd=cmd_build)


def cmd_build(args: argparse.Namespace) -> None:
    cfg = ProjectConfig.LoadFrom(Path.cwd() / "melontool.yml")
    cmd = [
        "dotnet",
        "clean",
        # '--no-restore',
        # "-c",
        # "Release",
        str(cfg.csproj),
    ]
    click.secho(
        _("Running {CMD}...").format(CMD=click.style(shlex.join(cmd), fg="yellow")),
        fg="green",
    )
    subprocess.run(cmd)
    cmd = [
        "dotnet",
        "build",
        # '--no-restore',
        "-c",
        "Release",
        str(cfg.csproj),
    ]
    click.secho(
        _("Running {CMD}...").format(CMD=click.style(shlex.join(cmd), fg="yellow")),
        fg="green",
    )
    p = subprocess.run(cmd)
    if cfg.ilrepack is not None:
        # click.secho(_('Waiting for subprocesses to flush files...'), fg='green')
        # for i in range(3):
        #     time.sleep(1.0)
        #     print(str(3-i)+'...', end='', flush=True)
        # print()
        cmd = cfg.ilrepack.gen_command(cfg, verbose=args.verbose)
        click.secho(
            _("Running {CMD}...").format(CMD=click.style(shlex.join(cmd), fg="yellow")),
            fg="green",
        )
        subprocess.run(cmd)
    if args.deploy:
        cmd_deploy(basedir=args.basedir)


def _cmd_add_file_ref(args: argparse.Namespace) -> None:
    cfg = ProjectConfig.LoadFrom(Path.cwd() / "melontool.yml")

    ol = len(cfg.file_refs)
    cfg.file_refs |= set(args.paths)

    if len(cfg.file_refs) > ol:
        cfg.save(PROJCFG_FILE)


def _reg_add_file_ref(subp: argparse._SubParsersAction):
    p = subp.add_parser(
        "file-ref", help=_("Adds an external assembly reference from the GAC.")
    )
    p.add_argument(
        "paths",
        nargs="+",
        type=Path,
        help=_("Assembly names (e.g. ../my/libraries/coollib.dll)"),
    )
    p.set_defaults(cmd=_cmd_add_file_ref)


def _cmd_add_system_ref(args: argparse.Namespace) -> None:
    cfg = ProjectConfig.LoadFrom(Path.cwd() / "melontool.yml")

    ol = len(cfg.system_refs)
    cfg.system_refs |= set(args.names)

    if len(cfg.system_refs) > ol:
        cfg.save(PROJCFG_FILE)


def _reg_add_system_ref(subp: argparse._SubParsersAction):
    p = subp.add_parser(
        "system-ref", help=_("Adds an external assembly reference from the GAC.")
    )
    p.add_argument(
        "names", nargs="+", type=str, help=_("Assembly names (e.g. System.Drawing)")
    )
    p.set_defaults(cmd=_cmd_add_system_ref)


def _cmd_add_game_ref(args: argparse.Namespace) -> None:
    cfg = ProjectConfig.LoadFrom(Path.cwd() / "melontool.yml")

    ol = len(cfg.game_refs)
    cfg.game_refs |= set(args.names)

    if len(cfg.game_refs) > ol:
        cfg.save(PROJCFG_FILE)


def _reg_add_game_ref(subp: argparse._SubParsersAction):
    p = subp.add_parser(
        "game-ref",
        help=_("Adds an assembly reference to an assembly the game itself provides."),
    )
    p.add_argument(
        "names", nargs="+", type=str, help=_("Assembly names (e.g. Assembly-CSharp)")
    )
    p.set_defaults(cmd=_cmd_add_game_ref)


def _cmd_add_mod_ref(args: argparse.Namespace) -> None:
    cfg = ProjectConfig.LoadFrom(Path.cwd() / "melontool.yml")

    ol = len(cfg.mod_refs)
    cfg.mod_refs |= set(args.names)

    if len(cfg.mod_refs) > ol:
        cfg.save(PROJCFG_FILE)


def _reg_add_mod_ref(subp: argparse._SubParsersAction):
    p = subp.add_parser(
        "mod-ref",
        help=_(
            "Adds a reference to an assembly that MelonLoader would treat as a mod."
        ),
    )
    p.add_argument(
        "names", nargs="+", type=str, help=_("Assembly names (e.g. YourMomLib)")
    )
    p.set_defaults(cmd=_cmd_add_mod_ref)


def _cmd_add_plugin_ref(args: argparse.Namespace) -> None:
    cfg = ProjectConfig.LoadFrom(Path.cwd() / "melontool.yml")

    ol = len(cfg.plugin_refs)
    cfg.plugin_refs |= set(args.names)

    if len(cfg.plugin_refs) > ol:
        cfg.save(PROJCFG_FILE)


def _reg_add_plugin_ref(subp: argparse._SubParsersAction):
    p = subp.add_parser(
        "plugin-ref",
        help=_(
            "Adds a reference to an assembly that MelonLoader would treat as a plugin."
        ),
    )
    p.add_argument(
        "names", nargs="+", type=str, help=_("Assembly names (e.g. YourMomLib)")
    )
    p.set_defaults(cmd=_cmd_add_plugin_ref)


def _cmd_add_project_ref(args: argparse.Namespace) -> None:
    cfg = ProjectConfig.LoadFrom(Path.cwd() / "melontool.yml")

    ol = len(cfg.project_refs)
    cfg.project_refs |= set(args.paths)

    if len(cfg.project_refs) > ol:
        cfg.save(PROJCFG_FILE)


def _cmd_add_embedded_resource(args: argparse.Namespace) -> None:
    cfg = ProjectConfig.LoadFrom(Path.cwd() / "melontool.yml")

    ol = len(cfg.embedded_resources)
    cfg.embedded_resources |= set(args.paths)

    if len(cfg.embedded_resources) > ol:
        cfg.save(PROJCFG_FILE)


def _reg_add_package(subp: argparse._SubParsersAction):
    p = subp.add_parser("package", help=_("Adds a reference to a NuGet package."))
    p.add_argument("name", type=str, help=_("NuGet package ID"))
    p.add_argument(
        "--version", type=str, help=_("Version of the package"), default=None
    )
    p.set_defaults(cmd=_cmd_add_package)


def _cmd_add_package(args: argparse.Namespace) -> None:
    cfg = ProjectConfig.LoadFrom(Path.cwd() / "melontool.yml")

    cfg.nuget_packages[args.name] = args.version

    cfg.save(PROJCFG_FILE)


def _reg_add_project_ref(subp: argparse._SubParsersAction):
    p = subp.add_parser(
        "project-ref", help=_("Adds a reference to a project directory.")
    )
    p.add_argument(
        "paths", nargs="+", type=Path, help=_("Assembly names (e.g. ./MyCoolMod.Core)")
    )
    p.set_defaults(cmd=_cmd_add_project_ref)


def _reg_add_embedded_resource(subp: argparse._SubParsersAction):
    p = subp.add_parser(
        "embedded-resource", help=_("Adds an embedded resource to the project.")
    )
    p.add_argument(
        "paths",
        nargs="+",
        type=Path,
        help=_("Paths to resources (e.g. Images/MyCoolMod.png)"),
    )
    p.set_defaults(cmd=_cmd_add_embedded_resource)


def _reg_add(subp: argparse._SubParsersAction) -> None:
    p = subp.add_parser("add", help=_("Add something to the project."))
    sp = p.add_subparsers()
    _reg_add_embedded_resource(sp)
    _reg_add_file_ref(sp)
    _reg_add_game_ref(sp)
    _reg_add_mod_ref(sp)
    _reg_add_package(sp)
    _reg_add_plugin_ref(sp)
    _reg_add_project_ref(sp)
    _reg_add_system_ref(sp)


def _reg_deploy(subp: argparse._SubParsersAction) -> None:
    cmdp = subp.add_parser(
        "deploy",
        help=_("Install the built DLL into your game's Mods or Plugins folder."),
    )
    cmdp.add_argument(
        "--basedir",
        "--base-dir",
        "--melonloader.basedir",
        "-B",
        type=str,
        default=None,
        help="See --melonloader.basedir (https://github.com/LavaGang/MelonLoader)",
    )
    cmdp.set_defaults(cmd=handle_deploy)


class InvalidProjectTypeException(Exception):
    pass


def handle_deploy(args: argparse.Namespace) -> None:
    cmd_deploy(basedir=args.basedir)


def cmd_deploy(basedir: Optional[str] = None) -> None:
    proj = ProjectConfig.LoadFrom(Path.cwd() / "melontool.yml")
    game_path = proj.game_path
    if basedir is not None:
        game_path = game_path / basedir
    if proj.ilrepack is None:
        srcfile = Path.cwd() / "dist" / DEFAULT_DNF / (proj.csproj.stem + ".dll")
    else:
        srcfile = (
            Path.cwd() / "dist" / DEFAULT_DNF / (proj.csproj.stem + ".repacked.dll")
        )
    outfile: Path
    match proj.type:
        case EProjectType.MOD:
            outfile = game_path / "Mods" / (proj.csproj.stem + ".dll")
        case EProjectType.PLUGIN:
            outfile = game_path / "Plugins" / (proj.csproj.stem + ".dll")
        case _:
            raise InvalidProjectTypeException()
    click.echo(
        _("Copying {srcfile} to {outfile}...").format(srcfile=srcfile, outfile=outfile)
    )
    shutil.copy(srcfile, outfile)


def _reg_generate(subp: argparse._SubParsersAction) -> None:
    cmdp = subp.add_parser("generate", help=_("Create csproj from melontool.yml"))
    cmdp.add_argument(
        "--steam-app-name",
        "-N",
        choices=[x.name.lower() for x in EGameID if x.value is not None],
        help=_("Steam App name of the target game"),
        default=None,
        nargs="?",
    )
    cmdp.add_argument(
        "--steam-app-id",
        "-I",
        type=int,
        help=_("Steam AppId of the target game"),
        default=None,
        nargs="?",
    )
    cmdp.add_argument(
        "--game-path",
        "-P",
        type=existingDirPath("--game-path/-I"),
        help=_("Installation path of the target game"),
        default=None,
        nargs="?",
    )
    cmdp.add_argument(
        "project", type=str, help=_("Path of project file"), default=None, nargs="?"
    )
    cmdp.set_defaults(cmd=cmd_generate)


def cmd_generate(args: argparse.Namespace) -> None:
    cfg = ProjectConfig.LoadFrom(Path.cwd() / "melontool.yml")
    if args.project is not None:
        cfg.csproj = args.project

    lcfg_dirty: bool = False
    cfg_dirty: bool = False
    if args.steam_app_name is not None:
        cfg.set_appname(args.steam_app_name)
        cfg_dirty = True
        lcfg_dirty = True
    if args.steam_app_id is not None:
        cfg.set_appid(args.steam_app_id)
        cfg_dirty = True
        lcfg_dirty = True
    if args.game_path is not None:
        cfg.localConfig.game_path = args.game_path
        lcfg_dirty = True

    ## Idiot checks
    assert cfg.game is not None, _("Could not find game specified")
    assert cfg.game.unityInfo is not None, _("Could not find game unityInfo")

    if cfg_dirty:
        cfg.save(Path("melontool.yml"))
    if lcfg_dirty:
        cfg.saveLocalConfigTo(LOCALPROJCFG_FILE)

    proj = etree.Element("Project", {"Sdk": "Microsoft.NET.Sdk"})
    projprops = etree.SubElement(proj, "PropertyGroup")
    props = {
        "TargetFramework": "net48",
        "Deterministic": "true",
        "LangVersion": "latest",
        "OutputPath": "dist",
    }
    if len(cfg.options):
        props["AllowUnsafeBlocks"] = (
            "true" if cfg.options.get("unsafe", False) else "false"
        )
        if cfg.options.get("nullable", True):
            props["Nullable"] = "enable"
        if (v := cfg.options.get("root-namespace")) is not None:
            props["RootNamespace"] = str(v)

    for k, v in props.items():
        etree.SubElement(projprops, k, {}).text = str(v)

    items = etree.SubElement(proj, "ItemGroup")
    refs = {}
    if len(cfg.project_refs) > 0:
        for projpath in sorted(cfg.project_refs, key=lambda x: x.name):
            etree.SubElement(items, "ProjectReference", {"Include": str(projpath)})

    def addref(path: Path, private: bool = False) -> None:
        if path.stem in BREAKS_SHIT:
            return
        ref = Reference()
        ref.include = path.stem  # getSigForDLL(path)
        ref.hintPath = path
        ref.private = private
        refs[ref.include] = ref
        if not path.is_file():
            click.secho(
                _("W: Could not find reference {REFNAME}").format(REFNAME=str(path)),
                fg="yellow",
            )

    # old and busted addref(cfg.game_path / 'MelonLoader' / 'MelonLoader.dll')
    # C:\Program Files (x86)\Steam\steamapps\common\ChilloutVR\MelonLoader\Managed
    # G:\SteamLibrary\steamapps\common\ChilloutVR\MelonLoader\net35
    ML_LIB = cfg.getMelonLoaderLibDir()
    MANAGED_DIR = cfg.getGameManagedDir()
    addref(ML_LIB / "MelonLoader.dll", private=True)
    addref(ML_LIB / "0Harmony.dll", private=True)
    managed_ref_dirs = [cfg.getGameManagedDir()]
    # for fileexe in cfg.game_path.glob("*.exe"):
    #     if game_managed.is_dir():
    #         managed_ref_dirs.append(game_managed)
    #         break
    managed_ref_dirs.append(cfg.getMelonLoaderLibDir())
    managed_ref_dirs.append(cfg.getMelonLoaderManagedDir())

    mod_ref_dir = cfg.game_path / "Mods"
    plugin_ref_dir = cfg.game_path / "Plugins"

    # for mrd in managed_ref_dirs:
    #     # click.secho(f'Looking for DLLs in {mrd}...')
    #     for path in mrd.glob("*.dll"):
    #         addref(path)

    mrd: Path
    for mrd in managed_ref_dirs:
        # click.secho(f'Looking for DLLs in {mrd}...')
        for path in mrd.glob("*.dll"):
            if path.name[:-4] in cfg.game_refs:
                addref(path, private=True)

    if mod_ref_dir.is_dir():
        for path in sorted(mod_ref_dir.glob("*.dll")):
            if path.name[:-4] in cfg.mod_refs:
                addref(path, private=True)

    if plugin_ref_dir.is_dir():
        for path in sorted(plugin_ref_dir.glob("*.dll")):
            if path.name[:-4] in cfg.plugin_refs:
                addref(path, private=True)

    for refid in sorted(cfg.system_refs):
        ref = Reference()
        ref.include = refid
        ref.addTo(items)

    for k, ref in sorted(refs.items()):
        ref.addTo(items)

    if cfg.ilrepack is not None:
        cfg.nuget_packages["ILRepack"] = None
        cfg.nuget_packages["ILRepack.MSBuild.Task"] = None

    if len(cfg.nuget_packages) > 0:
        packages = etree.SubElement(proj, "ItemGroup")
        for pkgid, vers in sorted(cfg.nuget_packages.items()):
            attr = {
                "Include": pkgid,
            }
            if vers is not None:
                attr["Version"] = vers
            etree.SubElement(packages, "PackageReference", attr)
    if len(cfg.embedded_resources) > 0:
        resources = etree.SubElement(proj, "ItemGroup")
        for item in sorted(cfg.embedded_resources):
            etree.SubElement(
                resources,
                "EmbeddedResource",
                {
                    "Include": str(item),
                },
            )

    # if cfg.ilrepack is not None:
    #     #<Target Name="ILRepack" AfterTargets="Build">
    #     target = etree.SubElement(proj, 'Target', {'Name':'ILRepack','AfterTargets':'Build'})
    #     inputAssemblies = etree.SubElement(target, 'ItemGroup')
    #     for assembly in [cfg.ilrepack.infile]+sorted(cfg.ilrepack.include_assemblies):
    #         etree.SubElement(inputAssemblies, 'InputAssemblies', {'Include':f'$(OutputPath)/{assembly}'})
    #
    #     # doNotInternalizeAssemblies = etree.SubElement(target,'ItemGroup')
    #     # etree.SubElement(doNotInternalizeAssemblies, 'DoNotInternalizeAssemblies', {'Include':f'$(OutputPath)/{cfg.ilrepack.infile}'})
    #
    #     #<ILRepack
    #     # Parallel="true"
    #     # Internalize="true"
    #     # InternalizeExclude="@(DoNotInternalizeAssemblies)"
    #     # InputAssemblies="@(InputAssemblies)"
    #     # TargetKind="Dll"
    #     # OutputFile="$(OutputPath)\$(AssemblyName).dll" />
    #     etree.SubElement(target, 'ILRepack',{
    #         'Parallel': 'true',
    #         # 'Internalize': 'true',
    #         # 'InternalizeExclude': '@(InputAssemblies)',
    #         'InputAssemblies': '@(InputAssemblies)',
    #         'TargetKind': 'Dll',
    #         'OutputFile': cfg.ilrepack.outfile,
    #     })

    tmpfile = cfg.csproj.with_suffix(".tmp")
    etree.ElementTree(proj).write(tmpfile, pretty_print=True)
    os.replace(tmpfile, cfg.csproj)
    sz = os.path.getsize(cfg.csproj)
    click.secho(
        _("Wrote {SIZE} to {CSPROJ}!").format(
            SIZE=humanfriendly.format_size(sz), CSPROJ=cfg.csproj
        ),
        fg="green",
    )


def _reg_convert(subp: argparse._SubParsersAction) -> None:
    cmdp: argparse._ActionsContainer = subp.add_parser(
        "convert", help=_("Create melontool.yml from a csproj file")
    )
    cmdp.add_argument(
        "--game-path-in-csproj",
        "-P",
        type=str,
        help=_("The Path to the game that the csproj references"),
        required=True,
    )
    cmdp.add_argument(
        "--game-path-in-reality",
        "-p",
        type=str,
        help=_("The Path to the game that is actually on your disk"),
        required=True,
    )
    cmdp.add_argument(
        "--steam-app-name",
        "-N",
        choices=[x.name.lower() for x in EGameID if x.value is not None],
        help=_("Steam App name of the target game"),
        default=None,
        nargs="?",
    )
    cmdp.add_argument(
        "--steam-app-id",
        "-I",
        type=int,
        help=_("Steam AppId of the target game"),
        default=None,
        nargs="?",
    )
    cmdp.add_argument(
        "csproj", type=existingFilePath("csproj"), help=_("The csproj to try to parse")
    )
    cmdp.set_defaults(cmd=cmd_convert)


def _import_vs2010_project(
    cfg: ProjectConfig, project: etree._Element, game_path_in_csproj: str
):
    for i, pg in enumerate(project.findall("PropertyGroup")):
        if i == 0:
            if (e := pg.find("RootNamespace")) is not None:
                cfg.options["root-namespace"] = e.text
            if (e := pg.find("AllowUnsafeBlocks")) is not None:
                cfg.options["unsafe"] = bool(e.text)
    for i, ig in enumerate(project.findall("ItemGroup")):
        match i:
            case 0:  # Refs
                for ref in ig.findall("Reference"):
                    hints = ref.findall("HintPath")
                    if "Include" in ref.attrib:
                        if len(hints):
                            hint = Path(hints[0].text)
                            handle_hint(cfg, hint, game_path_in_csproj)
                        else:
                            cfg.system_refs.add(ref.attrib["Include"])
            case 1:  # Compile stuff
                pass  # Ignored for now
            case 2:  # Project Refs
                for ref in ig.findall("ProjectReference"):
                    cfg.project_refs.add(ref.attrib["Include"])


def _import_vs2022_project(
    cfg: ProjectConfig, project: etree._Element, game_path_in_csproj: str
):
    for i, pg in enumerate(project.findall("PropertyGroup")):
        if i == 0:
            if (e := pg.find("RootNamespace")) is not None:
                cfg.options["root-namespace"] = e.text
            if (e := pg.find("AllowUnsafeBlocks")) is not None:
                cfg.options["unsafe"] = bool(e.text)
    for i, ig in enumerate(project.findall("ItemGroup")):
        match i:
            case 0:  # Refs
                for ref in ig.findall("Reference"):
                    hints = ref.findall("HintPath")
                    if "Include" in ref.attrib:
                        if len(hints):
                            hint = Path(hints[0].text)
                            handle_hint(cfg, hint, game_path_in_csproj)
                        else:
                            cfg.system_refs.add(ref.attrib["Include"])
            case 1:  # Compile stuff
                pass  # Ignored for now
            case 2:  # Project Refs
                for ref in ig.findall("ProjectReference"):
                    cfg.project_refs.add(ref.attrib["Include"])


def _import_dotnet_project(cfg, project, game_path_in_csproj: str):
    for i, pg in enumerate(project.findall("PropertyGroup")):
        if i == 0:
            if (e := pg.find("RootNamespace")) is not None:
                cfg.options["root-namespace"] = e.text
            if (e := pg.find("AllowUnsafeBlocks")) is not None:
                cfg.options["unsafe"] = bool(e.text)
    for i, ig in enumerate(project.findall("ItemGroup")):
        for ref in ig.findall("Reference"):
            hints = ref.findall("HintPath")
            if "Include" in ref.attrib:
                if len(hints):
                    hint = Path(hints[0].text)
                    handle_hint(cfg, hint, game_path_in_csproj)
                else:
                    cfg.system_refs.add(ref.attrib["Include"])
        for ref in ig.findall("ProjectReference"):
            cfg.project_refs.add(ref.attrib["Include"])


def handle_hint(cfg, hint: Path, game_path_in_csproj: str):
    shint = str(hint)
    parents = [x.name for x in hint.parents]
    if "Mods" in parents:
        cfg.mod_refs.add(hint.stem)
    elif "Plugins" in parents:
        cfg.plugin_refs.add(hint.stem)
    elif (
        "_Data" + os.sep + "Managed" in str(hint)
    ) or "MelonLoader" + os.sep + "Managed" in shint:
        cfg.game_refs.add(hint.stem)
    elif "MelonLoader" in shint:
        # Ignore, for now.
        pass
    else:
        cfg.file_refs.add(str(hint))


def cmd_convert(args: argparse.Namespace) -> None:
    cfg = ProjectConfig()
    if args.steam_app_name is not None:
        cfg.set_appname(args.steam_app_name)
    if args.steam_app_id is not None:
        cfg.set_appid(args.steam_app_id)

    ## Idiot checks
    assert cfg.game is not None, _("Could not find game specified")
    assert cfg.game.unityInfo is not None, _("Could not find game unityInfo")

    game_path_in_csproj: str = args.game_path_in_csproj
    cfg.localConfig.game_path = Path(game_path_in_csproj)

    cfg.csproj = args.csproj
    csproj: etree._ElementTree
    with open(args.csproj, "r") as f:
        csproj = etree.parse(f)

    root: etree._Element = csproj.getroot()
    roottag: str = root.tag
    if roottag.startswith("{http://schemas.microsoft.com/developer/msbuild/2003}"):
        for elem in root.getiterator():
            if not (
                isinstance(elem, etree._Comment)
                or isinstance(elem, etree._ProcessingInstruction)
            ):
                elem.tag = etree.QName(elem).localname
        etree.cleanup_namespaces(root)

    root = csproj.getroot()
    roottag = root.tag
    assert roottag == "Project", _("Invalid CSPROJ file.") + f" roottag=={roottag!r}"

    project: etree._Element = csproj.getroot()
    # DotNet
    if "Sdk" in project.attrib:
        match project.attrib["Sdk"]:
            case "Microsoft.NET.Sdk":
                _import_dotnet_project(cfg, project, game_path_in_csproj)
    # MSBUILD
    if "ToolsVersion" in project.attrib:
        match project.attrib["ToolsVersion"]:
            case "4.0":  # VS2010
                _import_vs2010_project(cfg, project, game_path_in_csproj)
            case "15.0":  # VS2022
                _import_vs2022_project(cfg, project, game_path_in_csproj)
    cfg.game.path = Path(args.game_path_in_reality)
    cfg.save(PROJCFG_FILE)
    cfg.saveLocalConfigTo(LOCALPROJCFG_FILE)


if __name__ == "__main__":
    main()
