from __future__ import annotations

import os
import shutil
from enum import IntEnum
from pathlib import Path
from typing import Any, Dict, Optional, Set, cast, List, Tuple

import click
import humanfriendly

from melontool.consts import DEFAULT_DNF
from melontool.gameids import EGameID, UnityGameInfo
from melontool.steamlibrary import SteamApp, SteamLibrary
from melontool.translation import I18NBuilder

__all__ = ["ProjectConfig"]

from melontool.i18n import getTxnFor  # isort: skip

t, _ = getTxnFor(__name__)

from ruamel.yaml import YAML as Yaml  # isort: skip

YAML = Yaml(typ="rt", pure=True)

ILREPACK_PATH = Path.cwd() / "ILRepack.exe"


class I18NConfig:
    def __init__(self) -> None:
        self.build: bool = False
        self.resource_file: Path = None
        self.enum_file: Path = None
        self.enum_namespace: str = ""
        self.enum_name: str = ""
        self.indent_chars: str = ""

    def deserialize(self, data: Dict[str, Any]) -> None:
        self.build = data["build"]
        self.resource_file = Path(data["resource"]["file"])
        enumcfg = data["enum"]
        self.enum_file = Path(enumcfg["file"])
        self.enum_namespace = enumcfg["namespace"]
        self.enum_name = enumcfg["name"]
        self.indent_chars = enumcfg.get("indent-chars", " " * 4)

    def serialize(self) -> Dict[str, Any]:
        cwd = Path.cwd()
        return {
            "build": self.build,
            "resource": {"file": str(self.resource_file.absolute().relative_to(cwd))},
            "enum": {
                "file": str(self.enum_file.absolute().relative_to(cwd)),
                "namespace": self.enum_namespace,
                "name": self.enum_name,
                "indent-chars": self.indent_chars,
            },
        }


class LocalProjectConfig:
    VERSION: int = 1

    def __init__(self) -> None:
        self.game_path: Optional[Path] = None

    def serialize(self) -> Dict[str, Any]:
        return {
            "version": self.VERSION,
            "paths": {
                "game": str(self.game_path) if self.game_path is not None else None
            },
        }

    def deserialize(self, data: Dict[str, Any]) -> None:
        # Put conversions here
        if data["version"] == self.VERSION:
            self.game_path = (
                Path(data["paths"]["game"])
                if data["paths"]["game"] is not None
                else None
            )

    def save(self, configfile: Path) -> None:
        tmppath = configfile.with_suffix(".tmp")
        with tmppath.open("w") as f:
            YAML.dump(self.serialize(), f)
        os.replace(tmppath, configfile)
        click.secho(
            _("Wrote {SIZE} to {FILENAME}.").format(
                FILENAME=configfile,
                SIZE=humanfriendly.format_size(os.path.getsize(configfile)),
            ),
            fg="green",
        )

    @staticmethod
    def LoadFrom(configpath: Path) -> ProjectConfig:
        pc = LocalProjectConfig()
        with configpath.open("r") as f:
            pc.deserialize(YAML.load(f))
        return pc


class EProjectType(IntEnum):
    MOD = 1
    PLUGIN = 2


def findAssemblyByNameIn(dir: Path, assemblyName: str) -> Optional[Path]:
    asmmatch = assemblyName.casefold()
    for assembly in dir.glob("*.dll"):
        if assembly.stem.casefold() == asmmatch:
            return assembly
        if assembly.name.casefold() == asmmatch:
            return assembly
    return None


def get_release_folder() -> Path:
    return Path("dist") / DEFAULT_DNF


def get_latest_package_dir(package: str) -> Path:
    # C:\Users\N3X15\.nuget\packages\ilrepack\2.0.18\tools
    pkgdir = Path.home() / ".nuget" / "packages" / package
    versions: Set[Tuple[int, ...]] = set()
    for dir in pkgdir.iterdir():
        if dir.is_dir():
            versions.add(tuple([int(x) for x in dir.name.split(".")]))
    avail_versions = sorted(versions, reverse=True)
    return pkgdir / ".".join(map(str, avail_versions[0]))


class ILRepackConfig:
    def __init__(self) -> None:
        self.ilrepack_path: Path = ILREPACK_PATH
        self.outfile: Path = Path(".")
        self.infile: Path = Path(".")
        self.include_assemblies: Set[str] = set()

    def gen_command(self, cfg: "ProjectConfig", verbose: bool = False) -> List[str]:
        return (
            [
                str(self.ilrepack_path),
                "/parallel",
                "/ndebug",
            ]
            + (
                [
                    "/verbose",
                ]
                if verbose
                else []
            )
            + [
                f"/lib:{cfg.getMelonLoaderLibDir()}",
                f"/lib:{cfg.getGameManagedDir()}",
                f"/lib:{cfg.getGameModsDir()}",
                f"/lib:{cfg.getGamePluginsDir()}",
                "/out:" + str(self.outfile),
                str(self.infile),
            ]
            + [
                str(findAssemblyByNameIn(get_release_folder(), x))
                for x in self.include_assemblies
            ]
        )

    def deserialize(self, data: dict) -> None:
        self.include_assemblies = set([x for x in data["assemblies"]])
        self.ilrepack_path = Path(
            data.get("ilrepack-path")
            or shutil.which("ILRepack")
            or (get_latest_package_dir("ILRepack") / "tools" / "ILRepack")
        )
        # self.infile = Path(data.get('in') or self.infile)
        # self.outfile = Path(data.get('out') or self.outfile)

    def serialize(self) -> dict:
        return {
            # 'in': self.infile.relative_to(get_release_folder()).as_posix(),
            # 'out': self.outfile.relative_to(get_release_folder()).as_posix(),
            "ilrepack-path": self.ilrepack_path.as_posix(),
            "assemblies": sorted([str(x) for x in self.include_assemblies]),
        }


class ProjectConfig:
    def __init__(self) -> None:
        self.game: Optional[SteamApp] = None
        self.csproj: Optional[Path] = None
        self.type: EProjectType = EProjectType.MOD
        self.options: Dict[str, str] = {}

        self.file_refs: Set[Path] = set()

        self.system_refs: Set[str] = set()
        self.game_refs: Set[str] = set()
        self.mod_refs: Set[str] = set()
        self.plugin_refs: Set[str] = set()
        self.project_refs: Set[Path] = set()
        self.nuget_packages: Dict[str, Optional[str]] = {}

        self.embedded_resources: Set[Path] = set()

        self.i18n: Optional[I18NConfig] = None
        self.ilrepack: Optional[ILRepackConfig] = None

        self.localConfig = LocalProjectConfig()

    def set_appid(self, appid: int) -> None:
        if self.game is None:
            self.game = SteamApp()
        ugi: UnityGameInfo = cast(
            UnityGameInfo,
            next(
                iter(
                    [
                        x.value
                        for x in EGameID
                        if x.value is not None and appid == x.value.appid
                    ]
                )
            ),
        )
        self.game.id = ugi.appid
        self.game.name = ugi.steam_dir_name or ugi.name
        self.game.unityInfo = ugi

    def set_appname(self, appname: str) -> None:
        if self.game is None:
            self.game = SteamApp()
        ugi: UnityGameInfo = cast(UnityGameInfo, EGameID[appname.upper()].value)
        self.game.id = ugi.appid
        self.game.name = ugi.steam_dir_name or ugi.name
        self.game.unityInfo = ugi

    def deserialize(self, data: Dict[str, Any]) -> None:
        self.csproj = Path(data["csproj"])

        if self.game is None:
            steam = SteamLibrary()
            steam.load()
            self.game = steam.get(int(data["gameid"]))
            self.game.unityInfo = next(
                iter(
                    [
                        x.value
                        for x in EGameID
                        if x.value is not None and self.game.id == x.value.appid
                    ]
                )
            )

            click.secho(
                _("Found steam app {GAME_NAME} {AT} {GAME_PATH}").format(
                    GAME_NAME=click.style(str(self.game.name), fg="yellow"),
                    GAME_PATH=click.style(str(self.game.path), fg="white"),
                    AT=click.style("at", fg="green"),
                ),
                fg="green",
            )
            # self.set_appid( if 'gameid' in data else None)

        self.options = {}
        if "options" in data:
            for k, v in data["options"].items():
                self.options[k] = v

        if "depends" in data:
            self.file_refs = set([Path(x) for x in data.get("depends", [])])
        else:
            self.file_refs = set([Path(x) for x in data.get("file-refs", [])])

        self.mod_refs = set(data.get("mod-refs", []))

        if "references" in data.keys():
            self.system_refs = set(data.get("references", []))
        else:
            self.system_refs = set(data.get("system-refs", []))
        self.game_refs = set(data.get("game-refs", []))
        self.plugin_refs = set(data.get("plugin-refs", []))
        self.project_refs = set([Path(x) for x in data.get("project-refs", [])])
        self.nuget_packages = {
            str(k): (str(v) if v is not None else None)
            for k, v in data.get("nuget-packages", {}).items()
        }
        self.type = EProjectType[data.get("type", "MOD").upper()]

        self.embedded_resources = set(
            [Path(x) for x in data.get("embedded-resources", [])]
        )

        if "i18n" in data:
            self.i18n = I18NConfig()
            self.i18n.deserialize(data["i18n"])
        if "ilrepack" in data:
            self.ilrepack = ILRepackConfig()
            self.ilrepack.infile = get_release_folder() / (self.csproj.stem + ".dll")
            self.ilrepack.outfile = get_release_folder() / (
                self.csproj.stem + ".repacked.dll"
            )
            self.ilrepack.deserialize(data["ilrepack"])

    @property
    def game_path(self) -> Path:
        return self.localConfig.game_path or self.game.path

    def build_i18n(self) -> None:
        if self.i18n.build:
            i18n = I18NBuilder()
            with open("i18n.yml", "r") as f:
                i18n.fromDict(YAML.load(f))
            i18n.genEnum(
                self.i18n.enum_file,
                self.i18n.enum_name,
                self.i18n.enum_namespace,
                self.i18n.indent_chars,
            )
            with open(self.i18n.resource_file, "wb") as f:
                f.write(i18n.asBytes())

    def serialize(self) -> Dict[str, Any]:
        o: Dict[str, Any] = {
            "csproj": str(self.csproj.absolute().relative_to(Path.cwd())),
            "gameid": self.game.id,
            "type": self.type.name.lower(),
            "options": self.options,
        }
        if len(self.system_refs) > 0:
            o["system-refs"] = list(sorted(self.system_refs))
        if len(self.game_refs) > 0:
            o["game-refs"] = list(sorted(self.game_refs))
        if len(self.mod_refs) > 0:
            o["mod-refs"] = list(sorted(self.mod_refs))
        if len(self.plugin_refs) > 0:
            o["plugin-refs"] = list(sorted(self.plugin_refs))
        if len(self.project_refs) > 0:
            o["project-refs"] = list(sorted(self.project_refs))
        if len(self.nuget_packages) > 0:
            o["nuget-packages"] = {k: v for k, v in sorted(self.nuget_packages.items())}
        if len(self.file_refs) > 0:
            o["file-refs"] = list(
                sorted(
                    map(
                        str,
                        [
                            Path(x).absolute().relative_to(self.game.path)
                            for x in self.file_refs
                        ],
                    )
                )
            )
        if self.ilrepack is not None:
            o["nuget-packages"] = self.ilrepack.serialize()
        if len(self.embedded_resources) > 0:
            o["embedded-resources"] = list(
                sorted(filter(lambda x: x.as_posix(), self.embedded_resources))
            )
        return o

    def save(self, configfile: Path) -> None:
        tmppath = configfile.with_suffix(".tmp")
        with tmppath.open("w") as f:
            YAML.dump(self.serialize(), f)
        os.replace(tmppath, configfile)
        click.secho(
            _("Wrote {SIZE} to {FILENAME}.").format(
                FILENAME=configfile,
                SIZE=humanfriendly.format_size(os.path.getsize(configfile)),
            ),
            fg="green",
        )

    def loadLocalConfigFrom(self, path: Path) -> bool:
        self.localConfig = LocalProjectConfig.LoadFrom(path)
        return True

    def saveLocalConfigTo(self, path: Path) -> None:
        self.localConfig.save(path)

    @staticmethod
    def LoadFrom(configpath: Path) -> ProjectConfig:
        pc = ProjectConfig()
        with configpath.open("r") as f:
            pc.deserialize(YAML.load(f))
        localpath = configpath.with_suffix(".local")
        if localpath.is_file():
            pc.loadLocalConfigFrom(localpath)
        return pc

    def getMelonLoaderLibDir(self) -> Path:
        mllibdir: str
        if self.game.unityInfo.is_il2cpp:
            mllibdir = "net6"
        else:
            mllibdir = "net35"
        return self.game_path / "MelonLoader" / mllibdir

    def getMelonLoaderManagedDir(self) -> Path:
        return self.game_path / "MelonLoader" / "Managed"

    def getGameManagedDir(self) -> Path:
        return self.game_path / f"{self.game.unityInfo.exe_stem}_Data" / "Managed"

    def getGameModsDir(self):
        return self.game_path / "Mods"

    def getGamePluginsDir(self):
        return self.game_path / "Plugins"
