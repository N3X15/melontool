# MelonTool

MelonTool is a simple commandline tool for juggling CSPROJ references for MelonLoader mods.

[[_TOC_]]

## Installation

You have two options: Source install or binary install.

### Source Install

This requires [CPython 3.10](https://python.org) or higher.

```shell
$ pip install git+https://gitlab.com/Scottinator/melontool.git
```

### Binary Install

See [tags](https://gitlab.com/Scottinator/melontool/-/tags) for executables compiled by Nuitka for Windows x64.

## Usage

**You need .NET 8 to run this tool.**

### Creating a new Melontool Project File

```shell
# Create a new project dir
$ mkdir CVRMelonTest

# Move inside the directory
$ cd CVRMelonTest

# Initialize the melontool.yml.  This is a ChilloutVR project.
$ melontool init --steam-app-name=chilloutvr --csproj CVRMelonTest.csproj
```

### Generating the .CSProj File

```shell
# Move inside the directory
$ cd CVRMelonTest

# Generate the .csproj file specified in melontool.yml with the given options.
$ melontool generate
```

### Building the Project

```shell
# Move inside the directory
$ cd CVRMelonTest

# Build the project using dotnet.
$ melontool build
```

## FAQ

### Can I add more dependencies?

Yes. Each path is relative to your project and refers to a file at *compile-time*.

#### Mods
Finds `{name}.dll` in `GAME_PATH/Mods/`.

```shell
$ melontool add mod-ref BTKUILib
```

```yaml
mod-refs:
  - BTKUILib
  - UIExpansionKit
```

#### Plugins
Finds `{name}.dll` in `GAME_PATH/Plugins/`.

```shell
$ melontool add plugin-ref MyPlugin.Core
```

```yaml
plugin-refs:
  - MyPlugin.Core
```
#### Projects
Inserts a project reference into the `.csproj` references list.

```shell
$ melontool add project-ref ../My.Dependency/My.Dependency.csproj
```

```yaml
project-refs:
  - ../My.Dependency/My.Dependency.csproj
```

#### NuGet Packages
Inserts a NuGet PackageReference into the `.csproj` package references list.

```shell
$ melontool add package Newtonsoft.Json --version 15.0.3
```

```yaml
nuget-packages:
  Newtonsoft.Json: 15.0.3
```

#### Files
These will point to actual file paths on disk. Not recommended.

```shell
$ melontool add file-ref Mods/UIExpansionKit.dll
```

```yaml
file-refs:
  - Mods/UIExpansionKit.dll
  - Mods/RubyButtons.dll
```

#### System/GAC Assemblies

Yes.

```shell
$ melontool add system-ref System.Core
```


```yaml
system-refs:
  - System
  - System.Core
  - System.Xml.Linq
```

### Can I add embedded resources?

Not yet. You can temporarily modify the CSProj like you would normally, just don't run generate.

### Can I use ILMerge/ILRepack?

MelonTool supports ILRepack.

```yaml
ilrepack:
  # Assemblies in $(OutputPath)
  assemblies:
    - Websocket.Client
    - obs-websocket-dotnet
    - System.Reactive
    - System.Runtime.CompilerServices.Unsafe
    - System.Threading.Channels
    - System.Threading.Tasks.Extensions
    - System.ValueTuple
```

This will generate `MyMod.repacked.dll` in `$(OutputPath)`.

`melontool deploy` will detect and deploy this DLL instead of the unpacked version.

### What do I add to my commits?

You shouldn't add your generated `*.csproj` unless you have to edit it manually.  You should, however, add the `melontool.yml` file.

### Where are the docs?

```shell
$ melontool [command] --help
```

### OLD: Oh my god what is with all these dependencies?!

~~Every possible dependency in your MelonLoader Managed directory/Game Managed directory is added. These are trimmed by dotnet at compile-time, so don't worry about it too much.~~

~~Trying to come up with a system to see which actual packages are used is way outside the scope of this project. VS2022 offers a tool like this somewhere, but requires heavy code analysis I don't have the time nor resources to implement.~~

All dependencies must now be specified manually using the `system-refs` list, the `game-refs` list, or the `mod-refs` list in `melontool.yml`.
